# JCTF SEC

## Swagger
http://localhost:8120/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

## Development

### Profile Configuration
-Dspring.profiles.active=prd

https://stackoverflow.com/questions/39738901/how-do-i-activate-a-spring-boot-profile-when-running-from-intellij

### Local Environment

Run the Application

it will create the local database 

Acess using the following link: http://localhost:8120/h2-console

User and password can be found on application-loc.yml

After that you can create a new user through the following endpoint: http://localhost:8120/api/v1/authentications/register

Example of body UserDTO.java

```json
{
"username": "test",
"userFullName": "Test",
"email": "test@jctf.com",
"password": "test"
}
```