package br.com.tts.jctfsec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JctfSecApplication {

	public static void main(String[] args) {
		SpringApplication.run(JctfSecApplication.class, args);
	}

}
