package br.com.tts.jctfsec.component;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


/**
 * @author Tiago Luiz Fernandes
 */

@Component
public class SecurityAppContext {

    public SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }

}