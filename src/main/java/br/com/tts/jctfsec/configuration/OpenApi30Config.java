package br.com.tts.jctfsec.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.context.annotation.Configuration;

/**
 * @author Tiago Luiz Fernandes
 */

@Configuration
@OpenAPIDefinition(info = @Info(
        title="JCTF SEC Definition",
        version = "1.0.0",
        contact = @Contact(
                name = "Tiago Luiz Fernandes",
                url = "https://www.linkedin.com/in/tiagoluizfernandes",
                email = "tiago.luiz.fernandes@gmail.com"),
        license = @License(
                name = "Apache 2.0",
                url = "https://www.apache.org/licenses/LICENSE-2.0.html"))
)
/*
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
*/
public class OpenApi30Config {
}
