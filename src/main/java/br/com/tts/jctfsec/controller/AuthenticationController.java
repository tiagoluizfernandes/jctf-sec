package br.com.tts.jctfsec.controller;

import br.com.tts.jctfsec.dto.LoginDTO;
import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.dto.response.AuthenticationResponse;
import br.com.tts.jctfsec.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tiago Luiz Fernandes
 */

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/authentications")
@CrossOrigin("*")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    @Tag(name = "Authentication" )
    @Operation(summary="Register a new user")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody UserDTO userDTO){
        return ResponseEntity.ok(authenticationService.register(userDTO));
    }

    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    @Tag(name = "Authentication" )
    @Operation(summary="Authenticate an existing user")
    public ResponseEntity<AuthenticationResponse> authenticate(
            @RequestBody LoginDTO loginDTO){
        return ResponseEntity.ok(authenticationService.authenticate(loginDTO));
    }

}
