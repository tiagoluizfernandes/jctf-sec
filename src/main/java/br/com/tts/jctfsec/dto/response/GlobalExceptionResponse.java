package br.com.tts.jctfsec.dto.response;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@Builder
public class GlobalExceptionResponse {
    private HttpStatus status;
    private String message;
    private String detail;
}
