package br.com.tts.jctfsec.entity;

import br.com.tts.jctfsec.enumeration.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Collection;
import java.util.List;

/**
 * @author Tiago Luiz Fernandes
 */

@Getter
@Setter
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="MCG_USUARIO", schema="MCG")

public class User implements UserDetails {

    @Id
    @Column(name="usuario")
    private String username;

    @Column(name = "nom_usuario")
    private String userFullName;

    @Column(name= "senha", nullable = false)
    private String password;

    //dat_vigencia_senha
    //dat_vencto_senha
    //dat_ultimo_login
    //estah_liberado
    //confirma_exclusao
    //idioma
    //telefone
    //nom_fans_ult_emp
    //nom_fans_ult_fil
    //qtd_maxima_login

    @Column(name = "email",  unique = true, nullable = false)
    private String email;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getPassword(){
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}

