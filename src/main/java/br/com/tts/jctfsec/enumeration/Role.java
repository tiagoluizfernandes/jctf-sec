package br.com.tts.jctfsec.enumeration;

/**
 * @author Tiago Luiz Fernandes
 */

public enum Role {

    USER,
    ADMIN

}
