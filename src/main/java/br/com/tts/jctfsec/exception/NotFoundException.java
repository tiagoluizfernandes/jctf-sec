package br.com.tts.jctfsec.exception;

import lombok.Getter;

@Getter
public abstract class NotFoundException extends RuntimeException {
    private final String what;
    public NotFoundException(String what) {
        super(what + " not found.");
        this.what = what;
    }
}
