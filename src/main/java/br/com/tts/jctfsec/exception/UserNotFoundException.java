package br.com.tts.jctfsec.exception;

/**
 * @author Tiago Luiz Fernandes
 */
public class UserNotFoundException extends NotFoundException{

    public UserNotFoundException() {
        super("User");
    }

}
