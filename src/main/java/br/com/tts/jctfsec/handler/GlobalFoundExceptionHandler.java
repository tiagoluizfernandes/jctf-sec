package br.com.tts.jctfsec.handler;

import br.com.tts.jctfsec.dto.response.GlobalExceptionResponse;
import br.com.tts.jctfsec.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalFoundExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<GlobalExceptionResponse> handlerNotFoundException(NotFoundException notFoundException){
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;

        GlobalExceptionResponse globalExceptionResponse =
                GlobalExceptionResponse.builder()
                        .message(notFoundException.getMessage())
                        .status(httpStatus)
                        .build();
        return new ResponseEntity<>(globalExceptionResponse, httpStatus);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<GlobalExceptionResponse> handlerException(Exception exception){
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        GlobalExceptionResponse globalExceptionResponse =
                GlobalExceptionResponse.builder()
                        .message(exception.getMessage())
                        .status(httpStatus)
                        .build();
        return new ResponseEntity<>(globalExceptionResponse,httpStatus);
    }

}
