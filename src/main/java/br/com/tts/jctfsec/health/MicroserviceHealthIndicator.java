package br.com.tts.jctfsec.health;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MicroserviceHealthIndicator implements HealthIndicator {

    @Value("${pom.version}")
    private String version;

    @Value("${pom.description}")
    private String description;


    @Override
    public Health health() {
        Health.Builder builder = new Health.Builder();

        Map<String, String> detailsMap = new HashMap<>();
        detailsMap.put("version", version);
        detailsMap.put("description", description);

        return builder
                .withDetails(detailsMap)
                .up()
                .build();
    }
}
