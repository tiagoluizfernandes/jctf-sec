package br.com.tts.jctfsec.mapper;

import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.entity.User;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {User.class})
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "role", constant = "USER")
    User toDomain(UserDTO userDTO);

    @InheritInverseConfiguration
    UserDTO toDTO(User user);
}
