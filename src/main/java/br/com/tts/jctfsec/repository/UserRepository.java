package br.com.tts.jctfsec.repository;

import br.com.tts.jctfsec.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Tiago Luiz Fernandes
 */

@Repository
public interface UserRepository extends JpaRepository<User,String> {
}
