package br.com.tts.jctfsec.service;

import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.dto.LoginDTO;
import br.com.tts.jctfsec.dto.response.AuthenticationResponse;
import br.com.tts.jctfsec.exception.UserNotFoundException;
import br.com.tts.jctfsec.mapper.UserMapper;
import br.com.tts.jctfsec.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author Tiago Luiz Fernandes
 */

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    private static final UserMapper userMapper = UserMapper.INSTANCE;

    public AuthenticationResponse register(UserDTO userDTO) {

        User user = userMapper.toDomain(userDTO);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepository.save(user);

        String jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
    public AuthenticationResponse authenticate(LoginDTO loginDTO) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getUsername(),
                        loginDTO.getPassword()));

        User user = userRepository.findById(loginDTO.getUsername()).orElseThrow(UserNotFoundException::new);

        String jwtToken = jwtService.generateToken(user);

        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

}
