package br.com.tts.jctfsec.service;

import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Tiago Luiz Fernandes
 */

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        return verifyIfExists(userId);
    }

    private User verifyIfExists(String userId) throws UsernameNotFoundException{

        return userRepository.findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException("User not found for the userId " + userId));
    }

}
