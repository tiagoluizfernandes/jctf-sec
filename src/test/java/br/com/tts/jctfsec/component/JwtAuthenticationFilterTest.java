package br.com.tts.jctfsec.component;

import br.com.tts.jctfsec.configuration.ApplicationConfig;
import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.enumeration.Role;
import br.com.tts.jctfsec.security.SecurityConfiguration;
import br.com.tts.jctfsec.service.JwtService;
import br.com.tts.jctfsec.service.UserService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * @author Tiago Luiz Fernandes
 */

@ExtendWith(SpringExtension.class)
@Import(ApplicationConfig.class)
@ContextConfiguration(classes = {SecurityConfiguration.class, JwtAuthenticationFilter.class, JwtService.class})
class JwtAuthenticationFilterTest {

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @MockBean
    private UserService userService;
    @Autowired
    private JwtService jwtService;
    @Mock
    private UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
    @Mock
    private SecurityAppContext securityAppContext;

    private HttpServletRequest request;
    private HttpServletResponse response;
    private FilterChain filterChain;

    @BeforeEach
    public void before() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);

        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    @SneakyThrows
    void testDoFilterInternalNullToken(){

        when(request.getHeader(AUTHORIZATION)).thenReturn(null);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(securityAppContext, userService, usernamePasswordAuthenticationToken );
        verify(filterChain).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());

    }

    @Test
    @SneakyThrows
    void testDoFilterInternalNotBearerToken(){

        when(request.getHeader(AUTHORIZATION)).thenReturn("NotBearerToken");

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(securityAppContext, userService, usernamePasswordAuthenticationToken );
        verify(filterChain).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());

    }

    @Test
    @SneakyThrows
    void testDoFilterInternalAuthenticationUserNull(){

        User user = new User();
        user.setUsername(null);
        user.setRole(Role.USER);

        String token = "Bearer " + jwtService.generateToken(user);

        when(request.getHeader(AUTHORIZATION)).thenReturn(token);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verifyNoInteractions(securityAppContext, userService, usernamePasswordAuthenticationToken );
        verify(filterChain).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());

    }

    @Test
    @SneakyThrows
    void testDoFilterInternalAuthenticationUserValidNotAuthenticated(){

        User user = new User();
        user.setUsername("dummy");
        user.setRole(Role.USER);

        String token = "Bearer " + jwtService.generateToken(user);

        when(request.getHeader(AUTHORIZATION)).thenReturn(token);
        when(userService.loadUserByUsername(anyString())).thenReturn(user);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);

        assertNotNull("Security context not Authenticated", SecurityContextHolder.getContext().getAuthentication());

    }

    @Test
    @SneakyThrows
    void testDoFilterInternalAuthenticationUserValidAuthenticated(){

        User user = new User();
        user.setUsername("dummy");
        user.setRole(Role.USER);

        String token = "Bearer " + jwtService.generateToken(user);

        when(request.getHeader(AUTHORIZATION)).thenReturn(token);
        when(userService.loadUserByUsername(anyString())).thenReturn(user);

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);

        assertNotNull("Security context not Authenticated", SecurityContextHolder.getContext().getAuthentication());

    }

    @Test
    @SneakyThrows
    void testDoFilterInternalAuthenticationWrongValidUser(){

        User user1 = new User();
        user1.setUsername("dummy1");
        user1.setRole(Role.USER);

        String token = "Bearer " + jwtService.generateToken(user1);

        when(request.getHeader(AUTHORIZATION)).thenReturn(token);

        User user2 = new User();
        user2.setUsername("dummy2");
        user2.setRole(Role.USER);


        when(userService.loadUserByUsername(anyString())).thenReturn(user2);

        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verify(filterChain).doFilter(request, response);
        assertNull(SecurityContextHolder.getContext().getAuthentication());

    }

}