package br.com.tts.jctfsec.component;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Tiago Luiz Fernandes
 */

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SecurityAppContext.class})
class SecurityAppContextTest {

    @Autowired
    private SecurityAppContext securityAppContext;

    @Test
    void testGetContextSuccess() {

        SecurityContext securityContext = securityAppContext.getContext();

        assertNotNull(securityContext);

    }

}