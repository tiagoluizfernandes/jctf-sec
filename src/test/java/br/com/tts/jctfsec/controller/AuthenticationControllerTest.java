package br.com.tts.jctfsec.controller;

import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.dto.response.AuthenticationResponse;
import br.com.tts.jctfsec.dto.LoginDTO;
import br.com.tts.jctfsec.exception.UserNotFoundException;
import br.com.tts.jctfsec.mapper.UserMapper;
import br.com.tts.jctfsec.service.AuthenticationService;
import br.com.tts.jctfsec.service.JwtService;
import br.com.tts.jctfsec.service.UserService;
import br.com.tts.jctfsec.support.template.LoginTemplates;
import br.com.tts.jctfsec.support.template.UserTemplates;
import br.com.tts.jctfsec.util.JsonConverter;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Tiago Luiz Fernandes
 */

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
    @WebMvcTest(AuthenticationController.class)
class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtService jwtService;

    @MockBean
    private UserService userService;

    @MockBean
    private AuthenticationService authenticationService;

    private static final String PATH = "/api/v1/authentications";

    private AuthenticationResponse authenticationResponse;

    private static final UserMapper userMapper = UserMapper.INSTANCE;

    @BeforeEach
    void setUp(){
        authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken("valid");
    }

    @Test
    @SneakyThrows
    void testRegisterSuccess() {

        UserDTO userDTO = userMapper.toDTO(UserTemplates.validUser());

        Mockito.when(authenticationService.register(userDTO)).thenReturn(authenticationResponse);

        mockMvc.perform(MockMvcRequestBuilders.post(PATH+"/register").contentType(MediaType.APPLICATION_JSON)
                        .content(JsonConverter.toJsonString(userDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token", Matchers.is(authenticationResponse.getToken())));
    }

    @Test
    @SneakyThrows
    void testAuthenticateSuccess() {

        LoginDTO loginDTO = LoginTemplates.validLoginDTO();

        Mockito.when(authenticationService.authenticate(loginDTO)).thenReturn(authenticationResponse);

        mockMvc.perform(MockMvcRequestBuilders.post(PATH+"/authenticate").contentType(MediaType.APPLICATION_JSON)
                        .content(JsonConverter.toJsonString(loginDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token", Matchers.is(authenticationResponse.getToken())));

    }

    @Test
    @SneakyThrows
    void testAuthenticateNotFoundException() {

        LoginDTO loginDTO = LoginTemplates.validLoginDTO();

        Mockito.when(authenticationService.authenticate(loginDTO)).thenThrow(new UserNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.post(PATH+"/authenticate").contentType(MediaType.APPLICATION_JSON)
                        .content(JsonConverter.toJsonString(loginDTO)))
                .andExpect(status().isNotFound());

    }

    @Test
    @SneakyThrows
    void testAuthenticateException() {

        LoginDTO loginDTO = LoginTemplates.validLoginDTO();

        Mockito.when(authenticationService.authenticate(loginDTO)).thenThrow(new RuntimeException());

        mockMvc.perform(MockMvcRequestBuilders.post(PATH+"/authenticate").contentType(MediaType.APPLICATION_JSON)
                        .content(JsonConverter.toJsonString(loginDTO)))
                .andExpect(status().isInternalServerError());

    }

}