package br.com.tts.jctfsec.health;

import br.com.tts.jctfsec.service.JwtService;
import br.com.tts.jctfsec.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest(MicroserviceHealthIndicator.class)
@TestPropertySource(properties = {
        "pom.version=test-0.0.0",
        "pom.description=test-Description"
})
public class MicroserviceHealthIndicatorTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtService jwtService;

    @MockBean
    private UserService userService;

    @Autowired
    private MicroserviceHealthIndicator healthIndicator;

    @Test
    public void testHealthIndicatorUp() {
        Health health = healthIndicator.health();
        Assertions.assertEquals("UP", health.getStatus().toString());
    }

    @Test
    public void testHealthIndicatorDetails() {
        Health health = healthIndicator.health();
        Assertions.assertEquals("test-0.0.0", health.getDetails().get("version"));
        Assertions.assertEquals("test-Description", health.getDetails().get("description"));
    }

}