package br.com.tts.jctfsec.mapper;

import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.enumeration.Role;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * @author Tiago Luiz Fernandes
 */
class UserMapperTest {

    private static final UserMapper userMapper = UserMapper.INSTANCE;

    @Test
    void testToDomainNull() {
        User user = userMapper.toDomain(null);

        assertNull(user);

        UserDTO userDTO = new UserDTO();

        userDTO.setUsername(null);
        userDTO.setUserFullName(null);
        userDTO.setEmail(null);
        userDTO.setPassword(null);

        user = userMapper.toDomain(userDTO);

        assertNull(user.getUsername());
        assertNull(user.getUserFullName());
        assertNull(user.getEmail());
        assertNull(user.getPassword());
        assertEquals(Role.USER, user.getRole());

    }

    @Test
    void testToDTONull() {
        UserDTO userDTO = userMapper.toDTO(null);

        assertNull(userDTO);

        User user = new User();

        user.setUsername(null);
        user.setUserFullName(null);
        user.setEmail(null);
        user.setPassword(null);
        user.setRole(null);

        userDTO = userMapper.toDTO(user);

        assertNull(userDTO.getUsername());
        assertNull(userDTO.getUserFullName());
        assertNull(userDTO.getEmail());
        assertNull(userDTO.getPassword());

    }

}