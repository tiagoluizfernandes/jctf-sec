package br.com.tts.jctfsec.service;

import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.dto.LoginDTO;
import br.com.tts.jctfsec.dto.response.AuthenticationResponse;
import br.com.tts.jctfsec.dto.UserDTO;
import br.com.tts.jctfsec.mapper.UserMapper;
import br.com.tts.jctfsec.repository.UserRepository;
import br.com.tts.jctfsec.support.template.LoginTemplates;
import br.com.tts.jctfsec.support.template.UserTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * @author Tiago Luiz Fernandes
 */
@ExtendWith(SpringExtension.class)
class AuthenticationServiceTest {
    @Mock
    private UserRepository userRepository;
    @Spy
    @InjectMocks
    private AuthenticationService authenticationService;

    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtService jwtService;

    @Mock
    private AuthenticationManager authenticationManager;
    private User user;
    private final String token = "token";

    private static final UserMapper userMapper = UserMapper.INSTANCE;

    @BeforeEach
    void setUp() {

        this.user = UserTemplates.validUser();

        Mockito.when(jwtService.generateToken(any())).thenReturn(token);
    }

    @Test
    void testRegisterSuccess(){

        Mockito.when(userRepository.save(any())).thenReturn(user);

        UserDTO userDTO = userMapper.toDTO(user);

        AuthenticationResponse authenticationResponse = authenticationService.register(userDTO);

        assertNotNull(authenticationResponse.getToken());
        assertEquals(token,authenticationResponse.getToken());

    }

    @Test
    void testAuthenticateSuccess(){

        Mockito.when(userRepository.findById(anyString())).thenReturn(Optional.ofNullable(user));

        LoginDTO loginDTO = LoginTemplates.validLoginDTO();

        AuthenticationResponse authenticationResponse =  authenticationService.authenticate(loginDTO);

        assertNotNull(authenticationResponse.getToken());
        assertEquals(token,authenticationResponse.getToken());
    }

}