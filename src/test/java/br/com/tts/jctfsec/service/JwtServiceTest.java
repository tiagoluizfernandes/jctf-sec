package br.com.tts.jctfsec.service;

import br.com.tts.jctfsec.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Tiago Luiz Fernandes
 */

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {JwtService.class})
class JwtServiceTest {

    @Autowired
    private JwtService jwtService;

    @Test
    void testValidateToken() {

        User user = new User();
        user.setUsername("dummy");
        user.setEmail("dummy@gmail.com");

        String token = jwtService.generateToken(user);

        assertTrue(jwtService.validateToken(token,user));

    }

}