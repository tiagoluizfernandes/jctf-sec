package br.com.tts.jctfsec.service;

import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.repository.UserRepository;
import br.com.tts.jctfsec.support.template.UserTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * @author Tiago Luiz Fernandes
 */

@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Spy
    @InjectMocks
    private UserService userService;

    private User user;

    @BeforeEach
    void setUp() {
        this.user = UserTemplates.validUser();
    }

    @Test
    void testLoadUserByUsernameSuccess() {
        Mockito.when(userRepository.findById(anyString())).thenReturn(Optional.ofNullable(user));

        User userDetails = (User) userService.loadUserByUsername(user.getUsername());

        assertEquals(userDetails.getUsername(), user.getUsername());
        assertEquals(userDetails.getUsername(), user.getUsername());
        assertEquals(userDetails.getEmail(),user.getEmail());
        assertEquals(userDetails.getPassword(),user.getPassword());
        assertEquals(userDetails.getAuthorities(),user.getAuthorities());

        assertTrue(userDetails.isAccountNonExpired());
        assertTrue(userDetails.isAccountNonLocked());
        assertTrue(userDetails.isCredentialsNonExpired());
        assertTrue(userDetails.isEnabled());


    }

    @Test
    void testLoadUserByUsernameNotFound() {
        UsernameNotFoundException usernameNotFoundException =  new UsernameNotFoundException("User not found for the userId " + this.user.getUsername());

        Mockito.when(userRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.loadUserByUsername(this.user.getUsername()))
                .isInstanceOf(UsernameNotFoundException.class)
                .hasMessage(usernameNotFoundException.getMessage());

    }

}