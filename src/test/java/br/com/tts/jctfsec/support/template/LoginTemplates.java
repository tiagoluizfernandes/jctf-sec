package br.com.tts.jctfsec.support.template;

import br.com.tts.jctfsec.dto.LoginDTO;

/**
 * @author Tiago Luiz Fernandes
 */
public class LoginTemplates {
    public static LoginDTO validLoginDTO() {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername("tiago");
        loginDTO.setPassword("1234");

        return loginDTO;
    }
}
