package br.com.tts.jctfsec.support.template;

import br.com.tts.jctfsec.entity.User;
import br.com.tts.jctfsec.enumeration.Role;

/**
 * @author Tiago Luiz Fernandes
 */
public class UserTemplates {

    public static User validUser() {
        User user = new User();
        user.setUsername("tiago");
        user.setUserFullName("Tiago Luiz Fernandes");
        user.setEmail("email@email.com");
        user.setPassword("1234");
        user.setRole(Role.USER);

        return user;
    }

}
